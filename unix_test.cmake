ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})

find_program(CTEST_COVERAGE_COMMAND NAMES gcov)
find_program(CTEST_MEMORYCHECK_COMMAND NAMES valgrind)

set(CTEST_MEMORYCHECK_COMMAND_OPTIONS "--trace-children=yes --leak-check=full")

ctest_start(${CTEST_DASHBOARD_TYPE})
ctest_configure()
ctest_build()
ctest_test()
if(CTEST_COVERAGE_COMMAND)
  ctest_coverage()
endif()
if(CTEST_MEMORYCHECK_COMMAND)
  ctest_memcheck()
endif()
ctest_submit(HTTPHEADER "Authorization: Bearer $ENV{CDASH_TOKEN}")
