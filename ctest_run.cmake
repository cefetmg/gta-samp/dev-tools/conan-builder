
set(build_site "local-station")
if(DEFINED ENV{BUILD_SITE})
  set(build_site "$ENV{BUILD_SITE}")
endif()

set(dashboard_type "Experimental")
if(DEFINED ENV{CDASH_DASHBOARD_TYPE})
  set(dashboard_type $ENV{CDASH_DASHBOARD_TYPE})
endif()

set(CTEST_PROJECT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/project")

set(CTEST_SOURCE_DIRECTORY "${CTEST_PROJECT_DIRECTORY}/src")
set(CTEST_BINARY_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build")

set(CTEST_SITE ${build_site})
set(CTEST_DASHBOARD_TYPE ${dashboard_type})
set(CTEST_BUILD_NAME "${CMAKE_SYSTEM_NAME}-$ENV{BUILD_NAME}")
set(CTEST_CONFIGURATION_TYPE Debug)

set(CTEST_CHECKOUT_COMMAND
    "conan install -if ${CTEST_BINARY_DIRECTORY} ${CTEST_PROJECT_DIRECTORY}"
)
set(CTEST_CONFIGURE_COMMAND
    "conan build --configure -bf ${CTEST_BINARY_DIRECTORY} ${CTEST_PROJECT_DIRECTORY}"
)
set(CTEST_BUILD_COMMAND
    "conan build --build -bf ${CTEST_BINARY_DIRECTORY} ${CTEST_PROJECT_DIRECTORY}"
)

if(UNIX)
  include("${CMAKE_CURRENT_SOURCE_DIR}/unix_test.cmake")
elseif(WIN32)
  include("${CMAKE_CURRENT_SOURCE_DIR}/win32_test.cmake")
endif()
